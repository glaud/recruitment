package pl.smartfactor.recruitment.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import pl.smartfactor.recruitment.mongo.document.DrinkOutlet;
import pl.smartfactor.recruitment.mongo.repository.DrinkOutletRepository;

public class DrinkOutletServiceTest {

	private DrinkOutletService drinkOutletService;

	@Mock
	private FileReaderService fileReaderService;

	@Mock
	private DrinkOutletRepository drinkOutletRepository;

	private List<DrinkOutlet> drinkOutlets;

	private DrinkOutlet valid1;

	private DrinkOutlet valid2;

	@Before
	public void setUp() {
		drinkOutletService = new DrinkOutletService(fileReaderService, drinkOutletRepository);
		setUpDrinkOutlets();
	}

	private void setUpDrinkOutlets() {
		drinkOutlets = new ArrayList<>();
		drinkOutlets.add(new DrinkOutlet.Builder("test1").shopName("testShopName").type("GASTRONOMIA").build());
		drinkOutlets.add(new DrinkOutlet.Builder("test2").shopName("testShopName").type("").build());
		drinkOutlets.add(new DrinkOutlet.Builder("test3").shopName("testShopName").type("INNY").build());
		valid1 = new DrinkOutlet.Builder("test4").shopName("testShopName").type("HANDEL").build();
		valid2 = new DrinkOutlet.Builder("test5").shopName("testShopName2").type("HANDEL").build();
		drinkOutlets.add(valid1);
		drinkOutlets.add(valid2);

	}

	@Test
	public void filterDrinkOutletsTest() {
		List<DrinkOutlet> actualDrinkOutlets = drinkOutletService.filterDrinkOutlets(drinkOutlets);
		assertEquals(Arrays.asList(valid1, valid2), actualDrinkOutlets);
	}

}

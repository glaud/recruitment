package pl.smartfactor.recruitment.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.smartfactor.recruitment.mongo.document.DrinkOutlet;

public interface DrinkOutletRepository extends MongoRepository<DrinkOutlet, String> {

}

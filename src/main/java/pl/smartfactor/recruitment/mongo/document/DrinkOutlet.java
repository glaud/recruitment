package pl.smartfactor.recruitment.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@ToString
public class DrinkOutlet {

	@Id
	private String id;
	private String businessmanName;
	private String shopName;
	private Address address;
	private String type;

	private DrinkOutlet() {
	}

	public static class Builder {
		private String businessmanName;
		private String shopName;
		private Address address;
		private String type;

		public Builder(String businessmanName) {
			this.businessmanName = businessmanName;
		}

		public Builder shopName(String shopName) {
			this.shopName = shopName;
			return this;
		}

		public Builder address(Address address) {
			this.address = address;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public DrinkOutlet build() {
			DrinkOutlet drinkOutlet = new DrinkOutlet();
			drinkOutlet.businessmanName = this.businessmanName;
			drinkOutlet.shopName = this.shopName;
			drinkOutlet.address = this.address;
			drinkOutlet.type = this.type;
			return drinkOutlet;
		}

	}

}

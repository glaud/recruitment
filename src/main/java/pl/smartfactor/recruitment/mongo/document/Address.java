package pl.smartfactor.recruitment.mongo.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {

	private String city;
	private String street;
	private String houseNumber;
	private String apartmentNumber;
	private String description;

	private Address() {
	}

	public static class Builder {
		private String city;
		private String street;
		private String houseNumber;
		private String apartmentNumber;
		private String description;

		public Builder() {
		}

		public Builder city(String city) {
			this.city = city;
			return this;
		}

		public Builder street(String street) {
			this.street = street;
			return this;
		}

		public Builder houseNumber(String houseNumber) {
			this.houseNumber = houseNumber;
			return this;
		}

		public Builder apartmentNumber(String apartmentNumber) {
			this.apartmentNumber = apartmentNumber;
			return this;
		}

		public Builder description(String description) {
			this.description = description;
			return this;
		}

		public Address build() {
			Address address = new Address();
			address.city = this.city;
			address.street = this.street;
			address.houseNumber = this.houseNumber;
			address.apartmentNumber = this.apartmentNumber;
			address.description = this.description;
			return address;
		}

	}

}

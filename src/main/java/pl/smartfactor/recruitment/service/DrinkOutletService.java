package pl.smartfactor.recruitment.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import lombok.extern.slf4j.Slf4j;
import pl.smartfactor.recruitment.mongo.document.DrinkOutlet;
import pl.smartfactor.recruitment.mongo.repository.DrinkOutletRepository;

@Service
@Slf4j
public class DrinkOutletService {

	private FileReaderService fileReaderService;

	private DrinkOutletRepository drinkOutletRepository;

	@Autowired
	public DrinkOutletService(FileReaderService fileReaderService, DrinkOutletRepository drinkOutletRepository) {
		this.fileReaderService = fileReaderService;
		this.drinkOutletRepository = drinkOutletRepository;
	}

	public List<DrinkOutlet> findAll() {
		return drinkOutletRepository.findAll();
	}

	public DrinkOutlet findById(String id) {
		Optional<DrinkOutlet> optDrinkOutlet = drinkOutletRepository.findById(id);
		return optDrinkOutlet.orElse(null);
	}

	public void deleteById(String id) {
		drinkOutletRepository.deleteById(id);
	}

	public boolean edit(DrinkOutlet drinkOutlet, String id) {
		Optional<DrinkOutlet> optDrinkOutlet = drinkOutletRepository.findById(id);
		if (optDrinkOutlet.isPresent()) {
			drinkOutlet.setId(id);
			drinkOutletRepository.save(drinkOutlet);
			return true;
		}
		return false;
	}

	public void create(@RequestBody DrinkOutlet drinkOutlet) {
		drinkOutletRepository.save(drinkOutlet);
	}

	public List<DrinkOutlet> findDrinkOutlets() throws IOException {
		List<DrinkOutlet> drinkOutlets = new ArrayList<>();
		drinkOutlets = fileReaderService.readDrinkOutletsFromFile();
		int initSize = drinkOutlets.size();
		drinkOutlets = filterDrinkOutlets(drinkOutlets);
		int filteredSize = drinkOutlets.size();
		log.info("Filtered " + filteredSize + " from " + initSize + " drink outlets");
		drinkOutletRepository.saveAll(drinkOutlets);
		return drinkOutlets;
	}

	public List<DrinkOutlet> filterDrinkOutlets(List<DrinkOutlet> drinkOutlets) {
		drinkOutlets = drinkOutlets.stream().filter(drinkOutlet -> drinkOutlet.getType().equalsIgnoreCase("HANDEL"))
				.collect(Collectors.toList());
		return drinkOutlets;
	}

	public List<DrinkOutlet> sortDrinkShops() {
		List<DrinkOutlet> drinkOutlets = findAll();
		drinkOutlets.sort(Comparator.comparing(DrinkOutlet::getBusinessmanName));
		return drinkOutlets;
	}
}

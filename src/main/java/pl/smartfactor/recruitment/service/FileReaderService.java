package pl.smartfactor.recruitment.service;

import java.io.IOException;
import java.util.List;

import pl.smartfactor.recruitment.mongo.document.DrinkOutlet;

public interface FileReaderService {

	List<DrinkOutlet> readDrinkOutletsFromFile() throws IOException;
}

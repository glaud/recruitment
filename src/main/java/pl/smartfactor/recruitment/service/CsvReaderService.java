package pl.smartfactor.recruitment.service;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.smartfactor.recruitment.mongo.document.Address;
import pl.smartfactor.recruitment.mongo.document.DrinkOutlet;

@Slf4j
@Service
public class CsvReaderService implements FileReaderService {

	@Value("${file.url}")
	private String fileUrl;

	public List<DrinkOutlet> readDrinkOutletsFromFile() throws IOException {
		List<DrinkOutlet> drinkOutlets = new ArrayList<>();
		log.info("Reading from file " + fileUrl);
		Scanner scanner = new Scanner(new FileReader(fileUrl, Charset.forName("ISO-8859-2")));
		int rowsCounter = 0;
		if (scanner.hasNext()) {
			scanner.nextLine();
		}
		while (scanner.hasNext()) {
			rowsCounter++;
			String[] lineArray = scanner.nextLine().split(";", 8);
			DrinkOutlet drinkOutlet = buildDrinkOutlet(lineArray);
			drinkOutlets.add(drinkOutlet);
		}
		log.info("Read " + rowsCounter + " line from file");
		scanner.close();
		return drinkOutlets;
	}

	private DrinkOutlet buildDrinkOutlet(String[] lineArray) {
		Address address = buildAddress(lineArray);
		String businessName = lineArray[0].replaceAll("\"", "").trim();
		String shopName = lineArray[1].replaceAll("\"", "").trim();
		DrinkOutlet drinkOutlet = new DrinkOutlet.Builder(businessName).shopName(shopName).address(address)
				.type(lineArray[5].trim()).build();
		return drinkOutlet;
	}

	private Address buildAddress(String[] lineArray) {
		Address address = new Address.Builder().city(lineArray[2]).street(lineArray[3]).houseNumber(lineArray[4])
				.apartmentNumber(lineArray[6]).description(lineArray[7]).build();
		return address;
	}
}

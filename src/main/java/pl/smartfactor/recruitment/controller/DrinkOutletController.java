package pl.smartfactor.recruitment.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.smartfactor.recruitment.mongo.document.DrinkOutlet;
import pl.smartfactor.recruitment.service.DrinkOutletService;

@RestController
@RequestMapping(value = "outlets")
public class DrinkOutletController {

	private DrinkOutletService drinkOutletService;

	@Autowired
	public DrinkOutletController(DrinkOutletService drinkOutletService) {
		this.drinkOutletService = drinkOutletService;
	}

	@GetMapping("/saveShops")
	public ResponseEntity<String> findDrinkShop() {
		List<DrinkOutlet> drinkOutlets;
		try {
			drinkOutlets = drinkOutletService.findDrinkOutlets();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.toString());
		}
		return ResponseEntity.status(HttpStatus.OK).body("Saved " + drinkOutlets.size() + " DrinkOutlets");
	}

	@GetMapping("/sortedShops")
	public List<DrinkOutlet> sortDrinkShops() {
		return drinkOutletService.sortDrinkShops();
	}

	@GetMapping("/{id}")
	public DrinkOutlet find(@PathVariable("id") String id) {
		return drinkOutletService.findById(id);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") String id) {
		drinkOutletService.deleteById(id);
		return ResponseEntity.status(HttpStatus.OK).body("Deleted DrinkOutlet with id " + id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> edit(@RequestBody DrinkOutlet drinkOutlet, @PathVariable("id") String id) {
		if (drinkOutletService.edit(drinkOutlet, id)) {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body("Updated " + drinkOutlet);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("DrinkOutlet with id " + id + " not found");
	}

	@PostMapping
	public ResponseEntity<String> create(@RequestBody DrinkOutlet drinkOutlet) {
		drinkOutletService.create(drinkOutlet);
		return ResponseEntity.status(HttpStatus.OK).body("Created " + drinkOutlet);
	}

}

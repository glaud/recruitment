# Recruitment Task

Recruitment task for Smart Factor.

[Swagger](http://localhost:9000/swagger-ui.html)

[Sample Postman collection for testing](task.postman_collection.json)


Run MongoDB in docker:
`docker run -p 27017:27017 -v mongodbdata:/data/db mongo:latest`


To run project in Eclipse IDE you need to install [lombok](https://projectlombok.org/).
